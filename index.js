// Script to show version information from https://jdk.java.net

// built-in https.get() required to download HTML of the website.
const https = require('https');
// htmlparser2 required to sort through HTML for important text.
const htmlparser2 = require('htmlparser2');

// :: Editable variables for temporarily storing data.
// Determine if current text being read is a title.
let header = false;
// Determine if current text being read is a version number.
let link = false;
// Store one <h1> title temporarily to be printed.
let currentHeader = '';
// Store the <a> names of every application next to the title in this array.
let versions = [];
// Contain all new JDK version numbers to scan.
let jdks = [];

// :: Parser for reading the website landing page.
const homeParser = new htmlparser2.Parser({
  onopentag(name, attribs) {
    // If "<h1>" is seen, mark it to be placed in "currentHeader".
    if (name == 'h1') header = true;
    // If "<a ..." is seen...
    if (name == 'a') {
      // Mark the next piece of text as a link.
      link = true;
      // If link matches formatting of a JDK release URL, add it to the "jdks" array.
      // Formatting = "/[VERSION]/"
      if (attribs.href.match(/^\/[0-9]*\/$/g)) jdks.push(attribs.href);
    }
  },
  ontext(text) {
    if (header) {
      // Add the header to "currentHeader" to be printed.
      currentHeader = text;
      // Reset the boolean value.
      header = false;
    }
    if (link) {
      // Remove any newlines, and add it to the array.
      versions.push(text.replace(/\n/g, ' '));
      // Again, reset value to ensure only links are read.
      link = false;
    }
  },
  onclosetag(name) {
    // If "</h1>" is seen (end of a header)...
    if (name == 'h1') {
      // If the amount of items in the "Data" array is bigger
      // than zero, print both the "currentHeader" and said array.
      if (versions.length)
        console.log(` --> ${currentHeader}${versions.join(', ')}`);
      // If the amount of items is zero (so an empty array,
      // with a boolean value of "false"), print just the header
      // (with a ":: " to make it stand out.)
      else console.log(`:: ${currentHeader}`);

      // Reset the list of versions so it doesn't pile up.
      versions = [];
    } else return;
  },
});

// :: Parser for reading pages on each different JDK version.
// Editable variables for determining if a piece of text is a version title and build number.
let h1 = false;
let h2 = false;
const jdkParser = new htmlparser2.Parser({
  onopentag(name, attribs) {
    // If <h1> or <h2> are seen, set their values to true.
    if (name == 'h1') h1 = true;
    if (name == 'h2') h2 = true;
  },
  ontext(text) {
    // If this piece of text has been marked as a <h1>...
    if (h1) {
      // Toggle title variable.
      h1 = false;
      // Print the title above the build number.
      return console.log(`=> ${text}`);
    }
    // If the text has been marked as a <h2>...
    if (h2) {
      // Toggle heading variable.
      h2 = false;
      // Check if the text contains "Build ".
      if (text.match(/Build /gi)) {
        // Log the heading underneath the title.
        return console.log(` --> ${text}`);
      }
    }
  },
});

// Get the HTML of the homepage, and turn it into text.
https.get('https://jdk.java.net', (res) => {
  let source = '';
  // Add all text to the variable above.
  res.on('data', (chunk) => (source += chunk));
  // Once download completes...
  res.on('end', () => {
    // Put the HTML through the homepage parser.
    homeParser.write(source);
    // Note that all console output is handled by the HTML parser.
    homeParser.end();

    // :: Begin searching through each available JDK release page in ascending order.
    console.log('\n:: Available JDKs');
    // Order the jdks array from earliest to latest release.
    jdks.sort();

    // Fetch each JDK page, and print their build number and date.
    jdks.forEach((item) => {
      https.get('https://jdk.java.net' + item, (res) => {
        let jdkPage = '';
        res.on('data', (piece) => (jdkPage += piece));

        // Write HTML to the parser, where console output is handled.
        res.on('end', () => jdkParser.write(jdkPage));
      });
    });
  });

  // End parser after all prints have completed.
  jdkParser.end();
});
